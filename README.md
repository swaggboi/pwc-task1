# pwc-task1

## TASK #1: Pair Difference

You are given an array of integers @N and an integer $A. Write a
script to find find if there exists a pair of elements in the array
whose difference is $A. Print 1 if exists otherwise 0.
